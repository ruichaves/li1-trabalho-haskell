{-|
Module      : Tarefa5_2017li1g75
Description : Módulo da Tarefa 5 para LI1 17/18

Módulo para a realização da Tarefa 5 de LI1 em 2017/18.
-}
module Main where

import LI11718
import Tarefa1_2017li1g75
import Tarefa2_2017li1g75
import Tarefa3_2017li1g75
import Tarefa4_2017li1g75
import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game

type Estado = (Int,[Picture],Jogo,Int,Acao,(Float,Float),Float,Int,[Picture],[(Picture,Posicao)])

main :: IO ()
main = do menu1 <- loadBMP "Pictures/menu1.bmp"
          menu2 <- loadBMP "Pictures/menu2.bmp"
          selcarro <- loadBMP "Pictures/selecionarcarro.bmp"
          carro1 <- loadBMP "Pictures/carro1.bmp"
          carro2 <- loadBMP "Pictures/carro2.bmp"
          carro3 <- loadBMP "Pictures/carro3.bmp"
          carro4 <- loadBMP "Pictures/carro4.bmp"
          carro5 <- loadBMP "Pictures/carro5.bmp"
          carro6 <- loadBMP "Pictures/carro6.bmp"

          lava <- loadBMP "Pictures/lava.bmp"

          rectaVertical0 <- loadBMP "Pictures/rectaVertical0.bmp"
          rectaVertical1 <- loadBMP "Pictures/rectaVertical1.bmp"
          rectaVertical2 <- loadBMP "Pictures/rectaVertical2.bmp"
          rectaVertical3 <- loadBMP "Pictures/rectaVertical3.bmp"
          rectaHorizontal0 <- loadBMP "Pictures/rectaHorizontal0.bmp"
          rectaHorizontal1 <- loadBMP "Pictures/rectaHorizontal1.bmp"
          rectaHorizontal2 <- loadBMP "Pictures/rectaHorizontal2.bmp"
          rectaHorizontal3 <- loadBMP "Pictures/rectaHorizontal3.bmp"

          curvaNorte0 <- loadBMP "Pictures/curvaNorte0.bmp"
          curvaNorte1 <- loadBMP "Pictures/curvaNorte1.bmp"
          curvaNorte2 <- loadBMP "Pictures/curvaNorte2.bmp"
          curvaNorte3 <- loadBMP "Pictures/curvaNorte3.bmp"
          curvaSul0 <- loadBMP "Pictures/curvaSul0.bmp"
          curvaSul1 <- loadBMP "Pictures/curvaSul1.bmp"
          curvaSul2 <- loadBMP "Pictures/curvaSul2.bmp"
          curvaSul3 <- loadBMP "Pictures/curvaSul3.bmp"
          curvaEste0 <- loadBMP "Pictures/curvaEste0.bmp"
          curvaEste1 <- loadBMP "Pictures/curvaEste1.bmp"
          curvaEste2 <- loadBMP "Pictures/curvaEste2.bmp"
          curvaEste3 <- loadBMP "Pictures/curvaEste3.bmp"
          curvaOeste0 <- loadBMP "Pictures/curvaOeste0.bmp"
          curvaOeste1 <- loadBMP "Pictures/curvaOeste1.bmp"
          curvaOeste2 <- loadBMP "Pictures/curvaOeste2.bmp"
          curvaOeste3 <- loadBMP "Pictures/curvaOeste3.bmp"

          rampaNorte0 <- loadBMP "Pictures/rampaNorte0.bmp"
          rampaNorte1 <- loadBMP "Pictures/rampaNorte1.bmp"
          rampaNorte2 <- loadBMP "Pictures/rampaNorte2.bmp"
          rampaNorte3 <- loadBMP "Pictures/rampaNorte3.bmp"
          rampaSul0 <- loadBMP "Pictures/rampaSul0.bmp"
          rampaSul1 <- loadBMP "Pictures/rampaSul1.bmp"
          rampaSul2 <- loadBMP "Pictures/rampaSul2.bmp"
          rampaSul3 <- loadBMP "Pictures/rampaSul3.bmp"
          rampaEste0 <- loadBMP "Pictures/rampaEste0.bmp"
          rampaEste1 <- loadBMP "Pictures/rampaEste1.bmp"
          rampaEste2 <- loadBMP "Pictures/rampaEste2.bmp"
          rampaEste3 <- loadBMP "Pictures/rampaEste3.bmp"
          rampaOeste0 <- loadBMP "Pictures/rampaOeste0.bmp"
          rampaOeste1 <- loadBMP "Pictures/rampaOeste1.bmp"
          rampaOeste2 <- loadBMP "Pictures/rampaOeste2.bmp"
          rampaOeste3 <- loadBMP "Pictures/rampaOeste3.bmp"

          pecaIntersecao0 <- loadBMP "Pictures/pecaIntersecao0.bmp"
          pecaIntersecao1 <- loadBMP "Pictures/pecaIntersecao1.bmp"
          pecaIntersecao2 <- loadBMP "Pictures/pecaIntersecao2.bmp"
          pecaIntersecao3 <- loadBMP "Pictures/pecaIntersecao3.bmp"

          play ecraMenu black 60 (estadoMenuInicial [menu1,menu2,selcarro] [carro1,carro2,carro3,carro4,carro5,carro6] [lava,rectaVertical0,rectaVertical1,rectaVertical2,rectaVertical3,rectaHorizontal0,rectaHorizontal1,rectaHorizontal2,rectaHorizontal3,curvaNorte0,curvaNorte1,curvaNorte2,curvaNorte3,curvaSul0,curvaSul1,curvaSul2,curvaSul3,curvaEste0,curvaEste1,curvaEste2,curvaEste3,curvaOeste0,curvaOeste1,curvaOeste2,curvaOeste3,rampaNorte0,rampaNorte1,rampaNorte2,rampaNorte3,rampaSul0,rampaSul1,rampaSul2,rampaSul3,rampaEste0,rampaEste1,rampaEste2,rampaEste3,rampaOeste0,rampaOeste1,rampaOeste2,rampaOeste3,pecaIntersecao0,pecaIntersecao1,pecaIntersecao2,pecaIntersecao3]) desenha reageTeclas reageTempo

-- | Função 'ecraMenu', função que define o tamanho da Window a apresentar.

ecraMenu :: Display
ecraMenu = FullScreen


-- | Função 'estadoMenuInicial', função que vai incluir todas as imagens criadas para esta tarefa no estado e definir o Estado Inicial.

estadoMenuInicial :: [Picture] -> [Picture] -> [Picture] -> Estado
estadoMenuInicial xs ys zs = (0
                      , xs
                      , (Jogo {mapa = Mapa ((2,1),Este) [[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]
                                    ,[Peca Lava 0,Peca (Curva Norte) (-1),Peca Recta (-1),Peca (Curva Este) (-1),Peca Lava 0]
                                    ,[Peca Lava 0,Peca Recta (-1),Peca Lava 0,Peca Recta (-1),Peca Lava 0]
                                    ,[Peca Lava 0,Peca Recta (-1),Peca Lava 0,Peca Recta (-1),Peca Lava 0]
                                    ,[Peca Lava 0,Peca (Curva Oeste) (-1),Peca Recta (-1),Peca (Curva Sul) (-1),Peca Lava 0]
                                    ,[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]]
                                    , pista  = Propriedades {k_atrito = 2,k_pneus = 3,k_acel = 4,k_peso = 2,k_nitro = 15,k_roda = 180}
                                    , carros = [Carro {posicao = (2,1),direcao = 45,velocidade = (0,0)}] 
                                    , nitros  = [0] 
                                    , historico = [[(2,1)]] })
                      , 0
                      , Acao {acelerar = False,travar = False,esquerda = False,direita = False,nitro = Nothing}
                      , (2,1)
                      , 0
                      , 0
                      , ys
                      , (picturesPosicoes zs (juntarOris (mapaToPePoOri (Mapa ((2,1),Este) (testaIntersecao  [[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
                                                                                                             [Peca Lava 0,Peca (Curva Norte) 0,Peca (Rampa Este) 0,Peca (Curva Este) 1,Peca Lava 0,Peca Lava 0,Peca Lava 0],
                                                                                                             [Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 1,Peca Lava 0,Peca Lava 0,Peca Lava 0],
                                                                                                             [Peca Lava 0,Peca (Curva Oeste) 0,Peca (Rampa Este) 0,Peca Recta 1,Peca Recta 1,Peca (Curva Este) 1,Peca Lava 0],
                                                                                                             [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Recta 1,Peca Lava 0,Peca Recta 1,Peca Lava 0],
                                                                                                             [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca (Curva Oeste) 1,Peca Recta 1,Peca (Curva Sul) 1,Peca Lava 0],
                                                                                                             [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]]
                                                                                                             [[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
                                                                                                             [Peca Lava 0,Peca (Curva Norte) 0,Peca (Rampa Este) 0,Peca (Curva Este) 1,Peca Lava 0,Peca Lava 0,Peca Lava 0],
                                                                                                             [Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 1,Peca Lava 0,Peca Lava 0,Peca Lava 0],
                                                                                                             [Peca Lava 0,Peca (Curva Oeste) 0,Peca (Rampa Este) 0,Peca Recta 1,Peca Recta 1,Peca (Curva Este) 1,Peca Lava 0],
                                                                                                             [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Recta 1,Peca Lava 0,Peca Recta 1,Peca Lava 0],
                                                                                                             [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca (Curva Oeste) 1,Peca Recta 1,Peca (Curva Sul) 1,Peca Lava 0],
                                                                                                             [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]] 
                                                                                                             (0,0))) (2,1) Este) 
                                                         [] 
                                                         (pePoToPePoOri (tabToPePo  [[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
                                                                                    [Peca Lava 0,Peca (Curva Norte) 0,Peca (Rampa Este) 0,Peca (Curva Este) 1,Peca Lava 0,Peca Lava 0,Peca Lava 0],
                                                                                    [Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 1,Peca Lava 0,Peca Lava 0,Peca Lava 0],
                                                                                    [Peca Lava 0,Peca (Curva Oeste) 0,Peca (Rampa Este) 0,Peca Recta 1,Peca Recta 1,Peca (Curva Este) 1,Peca Lava 0],
                                                                                    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Recta 1,Peca Lava 0,Peca Recta 1,Peca Lava 0],
                                                                                    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca (Curva Oeste) 1,Peca Recta 1,Peca (Curva Sul) 1,Peca Lava 0],
                                                                                    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]] (0,0))))))


------------------------------------------------------

-- | Função 'desenha', função que converte o estado numa imagem e a traduz na Window apresentada.

desenha :: Estado -> Picture
desenha (nm, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) | nm<3 = Pictures [Scale (0.17) (0.17) (Translate 0 0 (e !! (nm)))]
                           | otherwise = Pictures ((desenhaTabuleiro tab)++[Scale (0.045) (0.045) (Translate (((x)*2080)-5800) (((y)*2080)-6500) $ Rotate (direcaoCarro (c !! ic)) (b !! (j-1)))])

direcaoCarro :: Carro -> Float
direcaoCarro (Carro x y z) = realToFrac y

-- | Função 'desenhaTabuleiro', função utilizada em 'desenha' que a partir de uma lista (Picture,Posicao), que corresponde ao Tabuleiro, define as características de cada Picture e apresenta-as na Window.

desenhaTabuleiro :: [(Picture,Posicao)] -> [Picture]
desenhaTabuleiro [(pic,(x,y))] = [(Scale (0.045) (0.045) (Translate (fromIntegral((x*2080)-5800)) (fromIntegral((y*2080)-6500)) pic))]
desenhaTabuleiro tab = (Scale (0.045) (0.045) (Translate (((fst(posPicTab tab))*2080)-5800) (((snd(posPicTab tab))*2080)-6500) (fst(head tab)))) : desenhaTabuleiro (tail tab)


------------------------------------------------------

{- | Função 'posIniMapa', função que recebe um Mapa e retorna a Posição Inicial.
  
    == Exemplos de utilização:

    >>> posIniMapa Mapa ((2,3),Este) [[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca (Curva Norte) 0,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca (Curva Oeste) 0,Peca Recta 0,Peca Recta 0,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca (Curva Oeste) 0,Peca Recta 0,Peca (Curva Sul) 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]]
    (2.0,3.0)
-}
posIniMapa :: Mapa -> (Float,Float)
posIniMapa (Mapa ((x,y),z) tab) = (fromIntegral x,fromIntegral y)


-- | Função 'posPicTab', função utilizada em 'desenha' que recebe uma lista (Picture,Posicao) e retorna a Posição relativa da primeira Picture da lista.

posPicTab :: [(Picture,Posicao)] -> (Float,Float)
posPicTab ((pic,(x,y)):xs) = (fromIntegral x,fromIntegral y)

------------------------------------------------------

-- | Função 'reageTeclas', função que lida com os inputs.

reageTeclas :: Event -> Estado -> Estado
reageTeclas (EventKey (SpecialKey KeyDown) Down _ _)  (0, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (1, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab)
reageTeclas (EventKey (SpecialKey KeyRight) Down _ _) (0, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (1, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab)
reageTeclas (EventKey (SpecialKey KeyLeft) Down _ _)  (0, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (1, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab)
reageTeclas (EventKey (SpecialKey KeyUp) Down _ _)    (1, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (0, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab)
reageTeclas (EventKey (SpecialKey KeyRight) Down _ _) (1, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (0, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab)
reageTeclas (EventKey (SpecialKey KeyLeft) Down _ _)  (1, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (0, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab)
reageTeclas (EventKey (SpecialKey KeyEnter) Down _ _) (0, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (2, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab)
reageTeclas (EventKey (SpecialKey KeyEnter) Down _ _) (1, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (9, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab)

reageTeclas (EventKey (SpecialKey KeyF1) Down _ _)    (2, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (4,e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,1,b,tab)
reageTeclas (EventKey (SpecialKey KeyF2) Down _ _)    (2, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (4,e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,2,b,tab)
reageTeclas (EventKey (SpecialKey KeyF3) Down _ _)    (2, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (4,e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,3,b,tab)
reageTeclas (EventKey (SpecialKey KeyF4) Down _ _)    (2, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (4,e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,4,b,tab)
reageTeclas (EventKey (SpecialKey KeyF5) Down _ _)    (2, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (4,e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,5,b,tab)
reageTeclas (EventKey (SpecialKey KeyF6) Down _ _)    (2, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (4,e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,6,b,tab)

reageTeclas (EventKey (SpecialKey KeyUp) Down _ _)    (4, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (4,e,(Jogo m p c n h),ic,(Acao True d a r l),(x,y),t,j,b,tab)
reageTeclas (EventKey (SpecialKey KeyDown) Down _ _)  (4, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (4,e,(Jogo m p c n h),ic,(Acao u True a r l),(x,y),t,j,b,tab)
reageTeclas (EventKey (SpecialKey KeyRight) Down _ _) (4, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (4,e,(Jogo m p c n h),ic,(Acao u d True r l),(x,y),t,j,b,tab)
reageTeclas (EventKey (SpecialKey KeyLeft) Down _ _)  (4, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (4,e,(Jogo m p c n h),ic,(Acao u d a True l),(x,y),t,j,b,tab)

reageTeclas (EventKey (SpecialKey KeyUp) Up _ _)    (4, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (4,e,(Jogo m p c n h),ic,(Acao False d a r l),(x,y),t,j,b,tab)
reageTeclas (EventKey (SpecialKey KeyDown) Up _ _)  (4, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (4,e,(Jogo m p c n h),ic,(Acao u False a r l),(x,y),t,j,b,tab)
reageTeclas (EventKey (SpecialKey KeyRight) Up _ _) (4, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (4,e,(Jogo m p c n h),ic,(Acao u d False r l),(x,y),t,j,b,tab)
reageTeclas (EventKey (SpecialKey KeyLeft) Up _ _)  (4, e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (4,e,(Jogo m p c n h),ic,(Acao u d a False l),(x,y),t,j,b,tab)


reageTeclas _ s = s

------------------------------------------------------

-- | Função 'reageTempo', função que lida com o tempo de cada iteração.

reageTempo :: Float -> Estado -> Estado
reageTempo t' (0,e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (0,e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab)
reageTempo t' (1,e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (1,e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab)
reageTempo t' (2,e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab) = (2,e,(Jogo m p c n h),ic,(Acao u d a r l),(x,y),t,j,b,tab)

reageTempo t' (4,e,(Jogo m p c n h),ic,(Acao False False False False l),(x,y),t,j,b,tab)= (4,e, (atualiza (realToFrac t') (Jogo m p (listaCarros c ic (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic))) n h) ic (Acao False False False False l)),ic,(Acao False False False False l),posicaoCarro (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic)),t+t',j,b,tab)
reageTempo t' (4,e,(Jogo m p c n h),ic,(Acao True True True True l),(x,y),t,j,b,tab)= (4,e, (atualiza (realToFrac t') (Jogo m p (listaCarros c ic (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic))) n h) ic (Acao False False False False l)),ic,(Acao False False False False l),posicaoCarro (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic)),t+t',j,b,tab)
reageTempo t' (4,e,(Jogo m p c n h),ic,(Acao True False False False l),(x,y),t,j,b,tab) = (4,e, (atualiza (realToFrac t') (Jogo m p (listaCarros c ic (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic))) n h) ic (Acao True False False False l)),ic,(Acao True False False False l),posicaoCarro (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic)),t+t',j,b,tab)
reageTempo t' (4,e,(Jogo m p c n h),ic,(Acao False True False False l),(x,y),t,j,b,tab) = (4,e, (atualiza (realToFrac t') (Jogo m p (listaCarros c ic (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic))) n h) ic (Acao False True False False l)),ic,(Acao False True False False l),posicaoCarro (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic)),t+t',j,b,tab)
reageTempo t' (4,e,(Jogo m p c n h),ic,(Acao False False True False l),(x,y),t,j,b,tab) = (4,e, (atualiza (realToFrac t') (Jogo m p (listaCarros c ic (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic))) n h) ic (Acao False False True False l)),ic,(Acao False False True False l),posicaoCarro (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic)),t+t',j,b,tab)
reageTempo t' (4,e,(Jogo m p c n h),ic,(Acao False False False True l),(x,y),t,j,b,tab) = (4,e, (atualiza (realToFrac t') (Jogo m p (listaCarros c ic (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic))) n h) ic (Acao False False False True l)),ic,(Acao False False False True l),posicaoCarro (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic)),t+t',j,b,tab)
reageTempo t' (4,e,(Jogo m p c n h),ic,(Acao True True False False l),(x,y),t,j,b,tab)= (4,e, (atualiza (realToFrac t') (Jogo m p (listaCarros c ic (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic))) n h) ic (Acao True True False False l)),ic,(Acao True True False False l),posicaoCarro (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic)),t+t',j,b,tab)
reageTempo t' (4,e,(Jogo m p c n h),ic,(Acao True False True False l),(x,y),t,j,b,tab)= (4,e, (atualiza (realToFrac t') (Jogo m p (listaCarros c ic (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic))) n h) ic (Acao True False True False l)),ic,(Acao True False True False l),posicaoCarro (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic)),t+t',j,b,tab)
reageTempo t' (4,e,(Jogo m p c n h),ic,(Acao True False False True l),(x,y),t,j,b,tab)= (4,e, (atualiza (realToFrac t') (Jogo m p (listaCarros c ic (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic))) n h) ic (Acao True False False True l)),ic,(Acao True False False True l),posicaoCarro (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic)),t+t',j,b,tab)
reageTempo t' (4,e,(Jogo m p c n h),ic,(Acao False True True False l),(x,y),t,j,b,tab)= (4,e, (atualiza (realToFrac t') (Jogo m p (listaCarros c ic (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic))) n h) ic (Acao False True True False l)),ic,(Acao False True True False l),posicaoCarro (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic)),t+t',j,b,tab)
reageTempo t' (4,e,(Jogo m p c n h),ic,(Acao False True False True l),(x,y),t,j,b,tab)= (4,e, (atualiza (realToFrac t') (Jogo m p (listaCarros c ic (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic))) n h) ic (Acao False True False True l)),ic,(Acao False True False True l),posicaoCarro (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic)),t+t',j,b,tab)
reageTempo t' (4,e,(Jogo m p c n h),ic,(Acao False False True True l),(x,y),t,j,b,tab)= (4,e, (atualiza (realToFrac t') (Jogo m p (listaCarros c ic (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic))) n h) ic (Acao False False True True l)),ic,(Acao False False True True l),posicaoCarro (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic)),t+t',j,b,tab)
reageTempo t' (4,e,(Jogo m p c n h),ic,(Acao True True True False l),(x,y),t,j,b,tab) = (4,e, (atualiza (realToFrac t') (Jogo m p (listaCarros c ic (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic))) n h) ic (Acao True True True False l)),ic,(Acao True True True False l),posicaoCarro (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic)),t+t',j,b,tab)
reageTempo t' (4,e,(Jogo m p c n h),ic,(Acao True True False True l),(x,y),t,j,b,tab) = (4,e, (atualiza (realToFrac t') (Jogo m p (listaCarros c ic (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic))) n h) ic (Acao True True False True l)),ic,(Acao True True False True l),posicaoCarro (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic)),t+t',j,b,tab)
reageTempo t' (4,e,(Jogo m p c n h),ic,(Acao True False True True l),(x,y),t,j,b,tab) = (4,e, (atualiza (realToFrac t') (Jogo m p (listaCarros c ic (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic))) n h) ic (Acao True False True True l)),ic,(Acao True False True True l),posicaoCarro (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic)),t+t',j,b,tab)
reageTempo t' (4,e,(Jogo m p c n h),ic,(Acao False True True True l),(x,y),t,j,b,tab) = (4,e, (atualiza (realToFrac t') (Jogo m p (listaCarros c ic (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic))) n h) ic (Acao False True True True l)),ic,(Acao False True True True l),posicaoCarro (movimentarCarro ((!!) h ic) m (realToFrac t') ((!!) c ic)),t+t',j,b,tab)




movimentarCarro :: [Posicao] -> Mapa -> Tempo -> Carro -> Carro
movimentarCarro h (Mapa ((x,y),o) tab) t (Carro a b (vx,vy)) = justCarro h (movimenta tab t (Carro a b (vx,-vy)))

posicaoCarro :: Carro -> (Float,Float)
posicaoCarro (Carro (a,y) b (vx,vy)) = (realToFrac a,realToFrac y)

justCarro :: [Posicao] -> Maybe Carro -> Carro
justCarro h (Just c) = c
justCarro h Nothing = (Carro ((fromIntegral(fst(head (tail h)))),(fromIntegral(snd(head (tail h))))) 0 (0,0))

--------------------------------------------------------------------------------------------------------------------------------

-- | Função 'picturesPosicoes', função que transforma uma lista (Peca,Posicao,Orientacao) numa lista (Picture,Posicao), a ser traduzida como o Tabuleiro para a Window.

picturesPosicoes :: [Picture] -> [(Peca,Posicao,Orientacao)] -> [(Picture,Posicao)]
picturesPosicoes zs [(pe,(x,y),ori)] =  [((zs !! 0),(x,y))]
picturesPosicoes zs ((pe,(x,y),ori):xs) | pe == Peca Lava 0 = ((zs !! 0),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca Lava 90 = ((zs !! 41),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca Lava 91 = ((zs !! 42),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca Lava 92 = ((zs !! 43),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca Lava 93 = ((zs !! 44),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca Recta 0 && ori == Norte = ((zs !! 1),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca Recta 0 && ori == Sul   = ((zs !! 1),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca Recta 0 && ori == Este  = ((zs !! 5),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca Recta 0 && ori == Oeste = ((zs !! 5),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca Recta 1 && ori == Norte = ((zs !! 2),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca Recta 1 && ori == Sul   = ((zs !! 2),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca Recta 1 && ori == Este  = ((zs !! 6),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca Recta 1 && ori == Oeste = ((zs !! 6),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca Recta 2 && ori == Norte = ((zs !! 3),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca Recta 2 && ori == Sul   = ((zs !! 3),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca Recta 2 && ori == Este  = ((zs !! 7),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca Recta 2 && ori == Oeste = ((zs !! 7),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca Recta 3 && ori == Norte = ((zs !! 4),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca Recta 3 && ori == Sul   = ((zs !! 4),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca Recta 3 && ori == Este  = ((zs !! 8),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca Recta 3 && ori == Oeste = ((zs !! 8),(x,y)) : (picturesPosicoes zs xs)


                                        | pe == Peca (Curva Norte) 0 = ((zs !! 21),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Curva Norte) 1 = ((zs !! 22),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Curva Norte) 2 = ((zs !! 23),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Curva Norte) 3 = ((zs !! 24),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Curva Sul) 0   = ((zs !! 17),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Curva Sul) 1   = ((zs !! 18),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Curva Sul) 2   = ((zs !! 19),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Curva Sul) 3   = ((zs !! 20),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Curva Este) 0  = ((zs !! 13),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Curva Este) 1  = ((zs !! 14),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Curva Este) 2  = ((zs !! 15),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Curva Este) 3  = ((zs !! 16),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Curva Oeste) 0 = ((zs !! 9),(x,y))  : (picturesPosicoes zs xs)
                                        | pe == Peca (Curva Oeste) 1 = ((zs !! 10),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Curva Oeste) 2 = ((zs !! 11),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Curva Oeste) 3 = ((zs !! 12),(x,y)) : (picturesPosicoes zs xs)

                                        | pe == Peca (Rampa Norte) 0 = ((zs !! 25),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Rampa Norte) 1 = ((zs !! 26),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Rampa Norte) 2 = ((zs !! 27),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Rampa Norte) 3 = ((zs !! 28),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Rampa Sul) 0   = ((zs !! 29),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Rampa Sul) 1   = ((zs !! 30),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Rampa Sul) 2   = ((zs !! 31),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Rampa Sul) 3   = ((zs !! 32),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Rampa Este) 0  = ((zs !! 33),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Rampa Este) 1  = ((zs !! 34),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Rampa Este) 2  = ((zs !! 35),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Rampa Este) 3  = ((zs !! 36),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Rampa Oeste) 0 = ((zs !! 37),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Rampa Oeste) 1 = ((zs !! 38),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Rampa Oeste) 2 = ((zs !! 39),(x,y)) : (picturesPosicoes zs xs)
                                        | pe == Peca (Rampa Oeste) 3 = ((zs !! 40),(x,y)) : (picturesPosicoes zs xs)
                                        | otherwise                  = ((zs !! 0),(x,y))  : (picturesPosicoes zs xs)


---------------------------------AUXILIARES-------------------------------------

{- | Função 'tabToPePo', função que, de uma lista de lista de Pecas, associa cada Peca à sua Posição relativa no Tabuleiro.
  
    == Exemplos de utilização:

    >>> tabToPePo [[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca (Curva Norte) (1),Peca Recta (1),Peca (Curva Este) (1),Peca Lava 0],[Peca Lava 0,Peca Recta (1),Peca Lava 0,Peca Recta (1),Peca Lava 0],[Peca Lava 0,Peca Recta (1),Peca Lava 0,Peca Recta (1),Peca Lava 0],[Peca Lava 0,Peca (Curva Oeste) (1),Peca Recta (1),Peca (Curva Sul) (1),Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]] (0,0)
    [(Peca Lava 0,(0,0)),(Peca Lava 0,(1,0)),(Peca Lava 0,(2,0)),(Peca Lava 0,(3,0)),(Peca Lava 0,(4,0)),(Peca Lava 0,(0,1)),(Peca (Curva Norte) 1,(1,1)),(Peca Recta 1,(2,1)),(Peca (Curva Este) 1,(3,1)),(Peca Lava 0,(4,1)),(Peca Lava 0,(0,2)),(Peca Recta 1,(1,2)),(Peca Lava 0,(2,2)),(Peca Recta 1,(3,2)),(Peca Lava 0,(4,2)),(Peca Lava 0,(0,3)),(Peca Recta 1,(1,3)),(Peca Lava 0,(2,3)),(Peca Recta 1,(3,3)),(Peca Lava 0,(4,3)),(Peca Lava 0,(0,4)),(Peca (Curva Oeste) 1,(1,4)),(Peca Recta 1,(2,4)),(Peca (Curva Sul) 1,(3,4)),(Peca Lava 0,(4,4)),(Peca Lava 0,(0,5)),(Peca Lava 0,(1,5)),(Peca Lava 0,(2,5)),(Peca Lava 0,(3,5)),(Peca Lava 0,(4,5))]
-}
tabToPePo :: Tabuleiro -> Posicao -> [(Peca,Posicao)]
tabToPePo [x] (a,b) = pecay2 x (a,b)
tabToPePo (x:xs) (a,b) = pecay2 x (a,b) ++ tabToPePo xs (a,b+1)

{- | Função 'pecay2', função que, de uma lista de Pecas, associa cada Peca à sua Posição relativa no Tabuleiro.
  
    == Exemplos de utilização: 

    >>> pecay2 [Peca Lava 0,Peca (Curva Norte) (1),Peca Recta (1),Peca (Curva Este) (1),Peca Lava 0] (0,1)
    [(Peca Lava 0,(0,1)),(Peca (Curva Norte) 1,(1,1)),(Peca Recta 1,(2,1)),(Peca (Curva Este) 1,(3,1)),(Peca Lava 0,(4,1))]
-}
pecay2 :: [Peca] -> Posicao -> [(Peca,Posicao)]
pecay2 [x] (a,b) = [pecax2 x (a,b)]
pecay2 (x:xs) (a,b) = pecax2 x (a,b) : pecay2 xs (a+1,b)

{- | Função 'pecax2', função que associa uma Peca a uma Posição.
  
    == Exemplos de utilização:

    >>> pecax2 (Peca Lava 0) (2,3)
    (Peca Lava 0,(2,3))
-}
pecax2 :: Peca -> Posicao -> (Peca,Posicao)
pecax2 x y = (x,y)

--

{- | Função 'pePoToPePoOri', função que recebe uma lista (Peca,Posicao) e retorna cada elemento dessa lista associado à orientação Norte
  
    == Exemplos de utilização:

    >>> pePoToPePoOri [(Peca Lava 0,(0,0)),(Peca Lava 0,(1,0)),(Peca Lava 0,(2,0)),(Peca Lava 0,(3,0)),(Peca Lava 0,(4,0)),(Peca Lava 0,(0,1)),(Peca (Curva Norte) 1,(1,1)),(Peca Recta 1,(2,1)),(Peca (Curva Este) 1,(3,1)),(Peca Lava 0,(4,1)),(Peca Lava 0,(0,2)),(Peca Recta 1,(1,2)),(Peca Lava 0,(2,2)),(Peca Recta 1,(3,2)),(Peca Lava 0,(4,2)),(Peca Lava 0,(0,3)),(Peca Recta 1,(1,3)),(Peca Lava 0,(2,3)),(Peca Recta 1,(3,3)),(Peca Lava 0,(4,3)),(Peca Lava 0,(0,4)),(Peca (Curva Oeste) 1,(1,4)),(Peca Recta 1,(2,4)),(Peca (Curva Sul) 1,(3,4)),(Peca Lava 0,(4,4)),(Peca Lava 0,(0,5)),(Peca Lava 0,(1,5)),(Peca Lava 0,(2,5)),(Peca Lava 0,(3,5)),(Peca Lava 0,(4,5))]
    [(Peca Lava 0,(0,0),Norte),(Peca Lava 0,(1,0),Norte),(Peca Lava 0,(2,0),Norte),(Peca Lava 0,(3,0),Norte),(Peca Lava 0,(4,0),Norte),(Peca Lava 0,(0,1),Norte),(Peca (Curva Norte) 1,(1,1),Norte),(Peca Recta 1,(2,1),Norte),(Peca (Curva Este) 1,(3,1),Norte),(Peca Lava 0,(4,1),Norte),(Peca Lava 0,(0,2),Norte),(Peca Recta 1,(1,2),Norte),(Peca Lava 0,(2,2),Norte),(Peca Recta 1,(3,2),Norte),(Peca Lava 0,(4,2),Norte),(Peca Lava 0,(0,3),Norte),(Peca Recta 1,(1,3),Norte),(Peca Lava 0,(2,3),Norte),(Peca Recta 1,(3,3),Norte),(Peca Lava 0,(4,3),Norte),(Peca Lava 0,(0,4),Norte),(Peca (Curva Oeste) 1,(1,4),Norte),(Peca Recta 1,(2,4),Norte),(Peca (Curva Sul) 1,(3,4),Norte),(Peca Lava 0,(4,4),Norte),(Peca Lava 0,(0,5),Norte),(Peca Lava 0,(1,5),Norte),(Peca Lava 0,(2,5),Norte),(Peca Lava 0,(3,5),Norte),(Peca Lava 0,(4,5),Norte)]
-}
pePoToPePoOri :: [(Peca,Posicao)] -> [(Peca,Posicao,Orientacao)]
pePoToPePoOri [(pe,(x,y))] = [(pe,(x,y),Norte)]
pePoToPePoOri ((pe,(x,y)):xs) = (pe,(x,y),Norte): pePoToPePoOri xs

--

{- | Função 'mapaToPePoOri', função que recebe um Mapa e segundo a posição e orientação iniciais forma uma lista (Peca,Posicao,Orientacao), que contem as peças que correspondem ao caminho associadas à sua posiçao e orientação da peça seguinte.
  
    == Exemplos de utilização:

    >>> mapaToPePoOri (Mapa ((2,1),Este) [[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca (Curva Norte) (1),Peca Recta (1),Peca (Curva Este) (1),Peca Lava 0],[Peca Lava 0,Peca Recta (1),Peca Lava 0,Peca Recta (1),Peca Lava 0],[Peca Lava 0,Peca Recta (1),Peca Lava 0,Peca Recta (1),Peca Lava 0],[Peca Lava 0,Peca (Curva Oeste) (1),Peca Recta (1),Peca (Curva Sul) (1),Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]]) (2,1) Este
    [(Peca Recta 1,(2,1),Este),(Peca (Curva Este) 1,(3,1),Sul),(Peca Recta 1,(3,2),Sul),(Peca Recta 1,(3,3),Sul),(Peca (Curva Sul) 1,(3,4),Oeste),(Peca Recta 1,(2,4),Oeste),(Peca (Curva Oeste) 1,(1,4),Norte),(Peca Recta 1,(1,3),Norte),(Peca Recta 1,(1,2),Norte),(Peca (Curva Norte) 1,(1,1),Este)]
-}
mapaToPePoOri :: Mapa -> Posicao -> Orientacao -> [(Peca,Posicao,Orientacao)]
mapaToPePoOri (Mapa ((m,n),o) tab) (a,b) o1 = if qualPeca tab (proxPos peca (m,n) o) == Peca Lava 0
  then []
  else if (proxPos peca (m,n) o) == (a,b) && (proxOri peca o) == o1
    then [(peca,(m,n),proxOri peca o)]
    else (peca,(m,n),proxOri peca o) : mapaToPePoOri (Mapa ((proxPos peca (m,n) o),(proxOri peca o)) tab) (a,b) o1  
    where peca = ((!!) ((!!) tab n) m)

--

{- | Função 'whatPeca', caso específico para a função 'mapaToPeca'.
  
    == Exemplos de utilização:

    >>> whatPeca [[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca (Curva Norte) 0,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca (Curva Oeste) 0,Peca Recta 0,Peca Recta 0,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca (Curva Oeste) 0,Peca Recta 0,Peca (Curva Sul) 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]] (2,2)
    Peca Lava 0
-}
qualPeca :: Tabuleiro -> Posicao -> Peca
qualPeca t (m,n) = ((!!) ((!!) t n) m)

{- | Função 'juntarOris', função que substitui os elementos de uma lista [(Peca,Posicao,Orientacao)], que corresponde ao Tabuleiro e que têm todos orientação Norte, pelos elementos da mesma posição de uma outra lista [(Peca,Posicao,Orientacao)], que corresponde ao caminho e que têm todos a orientação da peça seguinte.
  
    == Exemplos de utilização:

    >>> juntarOris [(Peca Recta 1,(2,1),Este),(Peca (Curva Este) 1,(3,1),Sul),(Peca Recta 1,(3,2),Sul),(Peca Recta 1,(3,3),Sul),(Peca (Curva Sul) 1,(3,4),Oeste),(Peca Recta 1,(2,4),Oeste),(Peca (Curva Oeste) 1,(1,4),Norte),(Peca Recta 1,(1,3),Norte),(Peca Recta 1,(1,2),Norte),(Peca (Curva Norte) 1,(1,1),Este)]
                   []
                   [(Peca Lava 0,(0,0),Norte),(Peca Lava 0,(1,0),Norte),(Peca Lava 0,(2,0),Norte),(Peca Lava 0,(3,0),Norte),(Peca Lava 0,(4,0),Norte),(Peca Lava 0,(0,1),Norte),(Peca (Curva Norte) 1,(1,1),Norte),(Peca Recta 1,(2,1),Norte),(Peca (Curva Este) 1,(3,1),Norte),(Peca Lava 0,(4,1),Norte),(Peca Lava 0,(0,2),Norte),(Peca Recta 1,(1,2),Norte),(Peca Lava 0,(2,2),Norte),(Peca Recta 1,(3,2),Norte),(Peca Lava 0,(4,2),Norte),(Peca Lava 0,(0,3),Norte),(Peca Recta 1,(1,3),Norte),(Peca Lava 0,(2,3),Norte),(Peca Recta 1,(3,3),Norte),(Peca Lava 0,(4,3),Norte),(Peca Lava 0,(0,4),Norte),(Peca (Curva Oeste) 1,(1,4),Norte),(Peca Recta 1,(2,4),Norte),(Peca (Curva Sul) 1,(3,4),Norte),(Peca Lava 0,(4,4),Norte),(Peca Lava 0,(0,5),Norte),(Peca Lava 0,(1,5),Norte),(Peca Lava 0,(2,5),Norte),(Peca Lava 0,(3,5),Norte),(Peca Lava 0,(4,5),Norte)]
    [(Peca Lava 0,(0,0),Norte),(Peca Lava 0,(1,0),Norte),(Peca Lava 0,(2,0),Norte),(Peca Lava 0,(3,0),Norte),(Peca Lava 0,(4,0),Norte),(Peca Lava 0,(0,1),Norte),(Peca (Curva Norte) 1,(1,1),Este),(Peca Recta 1,(2,1),Este),(Peca (Curva Este) 1,(3,1),Sul),(Peca Lava 0,(4,1),Norte),(Peca Lava 0,(0,2),Norte),(Peca Recta 1,(1,2),Norte),(Peca Lava 0,(2,2),Norte),(Peca Recta 1,(3,2),Sul),(Peca Lava 0,(4,2),Norte),(Peca Lava 0,(0,3),Norte),(Peca Recta 1,(1,3),Norte),(Peca Lava 0,(2,3),Norte),(Peca Recta 1,(3,3),Sul),(Peca Lava 0,(4,3),Norte),(Peca Lava 0,(0,4),Norte),(Peca (Curva Oeste) 1,(1,4),Norte),(Peca Recta 1,(2,4),Oeste),(Peca (Curva Sul) 1,(3,4),Oeste),(Peca Lava 0,(4,4),Norte),(Peca Lava 0,(0,5),Norte),(Peca Lava 0,(1,5),Norte),(Peca Lava 0,(2,5),Norte),(Peca Lava 0,(3,5),Norte),(Peca Lava 0,(4,5),Norte)]
-}
juntarOris :: [(Peca,Posicao,Orientacao)] -> [(Peca,Posicao,Orientacao)] -> [(Peca,Posicao,Orientacao)] -> [(Peca,Posicao,Orientacao)]
juntarOris []                  []               ((pe2,(x2,y2),ori2):xs2)    = ((pe2,(x2,y2),ori2):xs2)
juntarOris [(pe,(x,y),ori)]    ((p,(a,b),o):bs) [(pe2,(x2,y2),ori2)]        = if (x,y) == (x2,y2)
                                                                            then ((p,(a,b),o):bs)++[(pe,(x,y),ori)]
                                                                            else ((p,(a,b),o):bs)++[(pe2,(x2,y2),ori2)]
juntarOris ((pe,(x,y),ori):xs) []               ((pe2,(x2,y2),ori2):xs2)    = if (x,y) == (x2,y2)
                                                                            then (juntarOris xs [] ([(pe,(x,y),ori)]++xs2))
                                                                            else juntarOris ((pe,(x,y),ori):xs) [(pe2,(x2,y2),ori2)] (xs2)
juntarOris ((pe,(x,y),ori):xs) ((p,(a,b),o):bs) [(pe2,(x2,y2),ori2)]        = if (x,y) == (x2,y2)
                                                                            then (juntarOris xs [] (((p,(a,b),o):bs)++[(pe,(x,y),ori)]))
                                                                            else []
juntarOris ((pe,(x,y),ori):xs) ((p,(a,b),o):bs) ((pe2,(x2,y2),ori2):xs2)    = if (x,y) == (x2,y2)
                                                                            then (juntarOris xs [] (((p,(a,b),o):bs)++[(pe,(x,y),ori)]++xs2))
                                                                            else juntarOris ((pe,(x,y),ori):xs) (((p,(a,b),o):bs)++[(pe2,(x2,y2),ori2)]) (xs2)

-------------------------------------

{- | Função 'pecasAVolta', cria uma lista com as Pecas do Tabuleiro nas posições imediatamente superior, inferior, à esquerda e à direita da Posicao dada.
                           Em alguns casos foi adicionado à lista a peça (Peca Lava 0) para facilitar a realização da função 'testaIntersecao' e 'testaIntersecaoAux'.
  
    == Exemplos de utilização:

    >>> pecasAVolta [[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca (Curva Norte) 0,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca (Curva Oeste) 0,Peca Recta 0,Peca Lava 90,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca (Curva Oeste) 0,Peca Recta 0,Peca (Curva Sul) 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]] (2,2)
    [Peca Recta 0,Peca Recta 0,Peca Recta 0,Peca Recta 0]
-}
pecasAVolta :: Tabuleiro -> Posicao -> [Peca]
pecasAVolta tab (x,y) | x == 0 && y == 0                                 =  [(Peca Lava 0),((!!) ((!!) tab y) (x+1)),((!!) ((!!) tab (y+1)) x)]
                      | x == 0 && y == (length tab)-1                    =  [(Peca Lava 0),((!!) ((!!) tab y) (x+1)),((!!) ((!!) tab (y-1)) x)]
                      | x == (length(head tab))-1 && y == 0              =  [(Peca Lava 0),((!!) ((!!) tab y) (x-1)),((!!) ((!!) tab (y+1)) x)]
                      | x == (length(head tab))-1 && y == (length tab)-1 =  [(Peca Lava 0),((!!) ((!!) tab y) (x-1)),((!!) ((!!) tab (y-1)) x)]
                      | x == 0                                           =  [(Peca Lava 0),((!!) ((!!) tab (y+1)) x),((!!) ((!!) tab (y-1)) x),((!!) ((!!) tab y) (x+1))]
                      | x == (length(head tab))-1                        =  [(Peca Lava 0),((!!) ((!!) tab (y+1)) x),((!!) ((!!) tab (y-1)) x),((!!) ((!!) tab y) (x-1))]
                      | y == 0                                           =  [(Peca Lava 0),((!!) ((!!) tab (y+1)) x),((!!) ((!!) tab y) (x+1)),((!!) ((!!) tab y) (x-1))]
                      | y == (length tab)-1                              =  [(Peca Lava 0),((!!) ((!!) tab (y-1)) x),((!!) ((!!) tab y) (x+1)),((!!) ((!!) tab y) (x-1))]
                      | otherwise                                        = [((!!) ((!!) tab (y+1)) x),((!!) ((!!) tab (y-1)) x),((!!) ((!!) tab y) (x+1)),((!!) ((!!) tab y) (x-1))]



{- | Função 'testaIntersecao', testa se alguma Peca corresponde a um ponto de interseção do caminho. 
                               Caso o seja substitui a Peca nessa posição pela Peca associada à imagem de uma interseção de altura correspondente.
  
    == Exemplos de utilização:

    >>> testaIntersecao [[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca (Curva Norte) 0,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca (Curva Oeste) 0,Peca Recta 0,Peca Recta 0,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca (Curva Oeste) 0,Peca Recta 0,Peca (Curva Sul) 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]] [[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca (Curva Norte) 0,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca (Curva Oeste) 0,Peca Recta 0,Peca Recta 0,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca (Curva Oeste) 0,Peca Recta 0,Peca (Curva Sul) 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]] (0,0))
    [[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca (Curva Norte) 0,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca (Curva Oeste) 0,Peca Recta 0,Peca Lava 90,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca (Curva Oeste) 0,Peca Recta 0,Peca (Curva Sul) 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]]
-}
testaIntersecao :: Tabuleiro -> Tabuleiro -> Posicao -> Tabuleiro
testaIntersecao tab [] (x,y) = []
testaIntersecao tab (ps:xs) (x,y) = (testaIntersecaoAux tab (ps:xs) (x,y)):(testaIntersecao tab (xs) (0,y+1))

{- | Função 'testaIntersecaoAux', função auxiliar da função 'testaIntersecao', que testa se alguma Peca corresponde a um ponto de interseção do caminho. 
                                  Caso o seja substitui a Peca nessa posição da linha do tabuleiro a ser testada pela Peca associada à imagem de uma interseção de altura correspondente.
  
    == Exemplos de utilização:

    >>> testaIntersecaoAux [[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca (Curva Norte) 0,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca (Curva Oeste) 0,Peca Recta 0,Peca Recta 0,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca (Curva Oeste) 0,Peca Recta 0,Peca (Curva Sul) 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]] [[Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],[Peca Lava 0,Peca (Curva Oeste) 0,Peca Recta 0,Peca Recta 0,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca (Curva Oeste) 0,Peca Recta 0,Peca (Curva Sul) 0,Peca Lava 0],[Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]] (0,2)
    [Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]
-}
testaIntersecaoAux :: Tabuleiro -> Tabuleiro -> Posicao -> [Peca]
testaIntersecaoAux tab ([p]:xs) (x,y) = if p==Peca Lava 0 
  then [p]
  else if elem (Peca Lava 0) (pecasAVolta tab (x,y))
    then [p]
    else [Peca Lava (altPecaInter p)]
testaIntersecaoAux tab ((p:ps):xs) (x,y) = if p==Peca Lava 0 
  then (p:(testaIntersecaoAux tab (ps:xs) ((x+1),y)))
  else if elem (Peca Lava 0) (pecasAVolta tab (x,y))
        then (p:(testaIntersecaoAux tab (ps:xs) ((x+1),y)))
        else (Peca Lava (altPecaInter p):(testaIntersecaoAux tab (ps:xs) ((x+1),y)))

{- | Função 'altPecaInter', define a altura da Peca Lava que vai corresponder à peça interseção.
  
    == Exemplos de utilização:

    >>> altPecaInter (Peca Recta 1)
    91
-}
altPecaInter :: Peca -> Int
altPecaInter (Peca _ x) = 90+x
